import json

def lambda_handler(event,context):
    return{
        "statusCode": 200,
        "headers":{
            "Content-Type": "application/json"
        },
        "body": json.dumps({
            "Message": "Hello world! This is a change I made to test my code pipeline"
        })
    }